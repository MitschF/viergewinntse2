package de.hdm_stuttgart.se2;

import de.hdm_stuttgart.se2.core.PlayerList;
import de.hdm_stuttgart.se2.fieldObjects.FieldObjectFactory;
import javafx.scene.paint.Color;
import org.junit.Before;
import org.junit.Test;



import static org.junit.Assert.assertEquals;

public class PlayerListTest {
    private PlayerList tester;
    private FieldObjectFactory factory;
    private Color color;

    @Before
    public void setup() {
        tester = new PlayerList(); //PlayerList is tested
        factory = FieldObjectFactory.getFactory(); //
        color =  Color.BROWN;
    }

    /**
     * Test for returning correct player name.
     */
    @Test
    public void GivenIdWhenGettingPlayerNameThenReturnCorrectPlayerNameFromArray() {

        tester.addPlayer(factory.createFieldObject(1, "PlayerName1", color));
        tester.addPlayer(factory.createFieldObject(2, "PlayerName2", color));
        tester.addPlayer(factory.createFieldObject(3, "PlayerName3", color));

        //assert statements
        assertEquals("PlayerName1", tester.getPlayerName(1));
        assertEquals("PlayerName2", tester.getPlayerName(2));
        assertEquals("PlayerName3", tester.getPlayerName(3));
    }
}

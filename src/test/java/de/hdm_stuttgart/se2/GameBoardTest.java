package de.hdm_stuttgart.se2;

import de.hdm_stuttgart.se2.core.GameBoard;
import org.junit.Test;

public class GameBoardTest {
    /**
     * Test for throwing ArrayIndexOutOfBoundsException when player enters incorrect column.
     */
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void GivenWrongInputWhenDroppingCoinThenException() {
        GameBoard gameBoard = new GameBoard(7, 5);
        gameBoard.createCells();

        gameBoard.dropCoin(8, 1);
    }
}

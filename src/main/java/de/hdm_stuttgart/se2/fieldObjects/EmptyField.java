package de.hdm_stuttgart.se2.fieldObjects;

import de.hdm_stuttgart.se2.interfaces_abstracts.FieldObject;
import javafx.scene.paint.Color;


public class EmptyField extends FieldObject {

    EmptyField() {
        super.color = Color.grayRgb(100);
        super.id = 0;
        super.name = "E";
    }


}

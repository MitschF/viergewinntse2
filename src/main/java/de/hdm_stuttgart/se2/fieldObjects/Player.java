package de.hdm_stuttgart.se2.fieldObjects;

import de.hdm_stuttgart.se2.interfaces_abstracts.FieldObject;
import javafx.scene.paint.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



public class Player extends FieldObject {
    private Logger log = LogManager.getLogger("logfile");

    private String name;

    Player(int id, String name, Color color) {
        this.name = name;
        super.color = color;
        super.id = id;
        log.debug("Spieler erstellt. Name: " + name + " ID: " + id + " Farbe: " + color.toString());
    }

    public String getName() {
        return this.name;
    }


}


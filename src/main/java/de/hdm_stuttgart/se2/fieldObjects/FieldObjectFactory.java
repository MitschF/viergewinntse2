package de.hdm_stuttgart.se2.fieldObjects;

import de.hdm_stuttgart.se2.interfaces_abstracts.IFieldObject;
import javafx.scene.paint.Color;


public class FieldObjectFactory {

    private static FieldObjectFactory instance;

    public static FieldObjectFactory getFactory() {
        if (instance == null) {
            instance = new FieldObjectFactory();
            return instance;
        } else {
            return instance;
        }
    }

    /**
     * @param id Spieler-ID, für Spieler1 ID:1, für Spieler2 ID:2
     * @param name Name als String von beiden Spielern, die ID zugeordnet werden
     * @param color Farben für beide Spieler, die ID zugeordnet werden
     * @return FieldObject Player
     */
    public IFieldObject createFieldObject(int id, String name, Color color) {
        return new Player(id, name, color);
    }

    public IFieldObject createFieldObject() {
        return new EmptyField();
    }

}

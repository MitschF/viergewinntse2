package de.hdm_stuttgart.se2.core;

import de.hdm_stuttgart.se2.fieldObjects.EmptyField;
import de.hdm_stuttgart.se2.fieldObjects.FieldObjectFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Stream;

public class GameBoard {


    private Logger log = LogManager.getLogger("logfile");

    private int fields;
    private int height;
    private int width;
    private Cell[][] cellArray;
    private int x;
    private int y;
    private int countTurns;

    private static GameBoard gameBoard;


    public static GameBoard getGameBoard() {
        if (gameBoard == null) {
            gameBoard = new GameBoard();
            return gameBoard;
        } else {
            return gameBoard;
        }
    }


    /**
     * Standart Konstruktor für GameBoard
     */
    private GameBoard() { //Standart Konstruktor für GameBoard
        this.width = 7;
        this.height = 6;
        this.fields = width * height;
        createCells();
    }

    /**
     * Konstruktor für das Spielfeld
     *
     * @param width  Breite des Spielfeldes
     * @param height Höhe des Spielfeldes
     */
    public GameBoard(int width, int height) {
        this.width = width;
        this.height = height;
        this.fields = width * height;
        createCells();
    }

    // Methoden ----------------------------------------------------------------------------------------------


    /**
     * Methode, welche ein 2-dimensionales Array erstellt und die einzelnen Positionen mit EmptyField Objekten füllt.
     */
    public void createCells() {
        this.cellArray = new Cell[width][height];
        FieldObjectFactory factory = FieldObjectFactory.getFactory();
        for (int i = 0; i <= height - 1; i++) {
            for (int j = 0; j <= width - 1; j++) {
                cellArray[j][i] = new Cell(factory.createFieldObject());
            }
        }
    }

    /**
     * Methode, welche das Spielfeld in der Konsole ausgibt
     */
    public void printGameboard() {
        StringBuilder traceLog = new StringBuilder("\n");
        for (int i = 0; i <= height - 1; i++) {
            for (int j = 0; j <= width - 1; j++) {
                traceLog.append(cellArray[j][i].fieldObject.getId()).append(" ");
            }
            traceLog.append("\n");
        }
        log.trace(traceLog);
        //System.out.println("Number of empty fields: " + countEmptyFields());

    }

    /**
     * Methode, mit welcher ein Spielstein gesetzt werden kann, welcher automatisch so weit wie möglich "nach unten" rutscht.
     *
     * @param spalte   Die Spalte, in welcher der Spielstein gesetzt werden soll
     * @param playerID Der Spieler, welcher den Stein setzt
     */
    public void dropCoin(int spalte, int playerID) {

        if (cellArray[spalte - 1][height - 1].fieldObject.getId() == 0) {
            cellArray[spalte - 1][height - 1].setPlayer(PlayerList.getPlayerList().getPlayerByID(playerID));
            x = spalte - 1;
            y = height - 1;
            this.countTurns++;
        } else if (cellArray[spalte - 1][0].fieldObject.getId() != 0) {
            log.debug("Spalte voll");
        } else {
            for (int j = 0; j < height; j++) {
                if (cellArray[spalte - 1][j].fieldObject.getId() != 0) {
                    cellArray[spalte - 1][j - 1].setPlayer(PlayerList.getPlayerList().getPlayerByID(playerID));
                    this.x = spalte - 1;
                    this.y = j - 1;
                    this.countTurns++;
                    break;
                }
            }
        }
    }


    /**
     * Gibt die Höhe des Spielfeldes zurück.
     *
     * @return Höhe des Spielfeldes als Integer.
     */
    public int getHeight() {
        return this.height;
    }

    /**
     * Gibt die Breite des Spielfeldes zurück.
     *
     * @return Breite des Spielfeldes als Integer.
     */
    public int getWidth() {
        return this.width;
    }

    /**
     * Gibt die Anzahl der Felder im Spielfeld zurück.
     *
     * @return Anzahl der Felder als Integer
     */
    public int getFields() {
        return this.fields;
    }

    /**
     * Gibt die X-Koordinate des Spielsteins zurück (in width vom Spielfeld).
     *
     * @return X-Koordinate des Spielsteins als Integer.
     */
    int getX() {
        return this.x;
    }

    /**
     * Settet die X-Koordinate des Spielsteins
     * @param zahl der X-Koordinate
     */
    public void setX(int zahl){this.x = zahl;}

    /**
     * Gibt die Y-Koordinate des Spielsteins zurück (in height vom Spielfeld).
     *
     * @return Y-Koordinate des Spielsteins als Integer.
     */
    public int getY() {
        return this.y;
    }

    /**
     * Settet die X-Koordinate des Spielsteins
     * @param zahl der Y-Koordinate
     */
    public void setY(int zahl){this.y=zahl;}

    /**
     * Gibt das Cell-Array des Spielfelds zurück um x und y eingeben zu können.
     *
     * @return Cell-Array mit x- und y-Eingabemöglichkeit und Methodenabruf.
     */
    public Cell[][] getCellArray() {
        return this.cellArray;
    }

    public int getCountTurns() {
        return this.countTurns;
    }
    public void setCountTurns(int zahl){this.countTurns = zahl; }

    public boolean getRowFull(int spalte) {
        return cellArray[spalte - 1][0].fieldObject.getId() != 0;
    }

    public int countEmptyFields() {
        //long startTime = System.nanoTime();
        return Stream.of(gameBoard.getCellArray())
                    .parallel()
                    .flatMap(Stream::of)
                    //.peek(e -> System.out.println(System.nanoTime() - startTime))
                    .filter(x -> x.fieldObject instanceof EmptyField)
                    .toArray()
                    .length;

    }
}
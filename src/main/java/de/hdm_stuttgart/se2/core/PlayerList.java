package de.hdm_stuttgart.se2.core;

import de.hdm_stuttgart.se2.interfaces_abstracts.IFieldObject;
import javafx.scene.paint.Color;


import java.util.ArrayList;

public class PlayerList {

    private ArrayList<IFieldObject> playerListList = new ArrayList<IFieldObject>();

    private static PlayerList playerlist;

    /**
     * Singleton für die Playerlist. Erzeugt eine Playerlist, wenn es noch keine gibt. Sonst gibt es die existierende zurück.
     *
     * @return existierende Playerlist
     */
    public static PlayerList getPlayerList() {
        if (playerlist == null) {
            playerlist = new PlayerList();
            return playerlist;
        } else {
            return playerlist;
        }
    }

    public PlayerList() {
    }

    // Methoden ---------------------------------------------------------------


    /**
     * Fügt ein IFieldObject > Player zur ArrayList playerListList hinzu.
     *
     * @param player FieldObject Player über die FieldObjectFactory.
     */
    public void addPlayer(IFieldObject player) {
        this.playerListList.add(player);
    }

    public void removePlayer(){
        this.playerListList.remove(0);
        this.playerListList.remove(0);
    }

    /**
     * Methode, welche die ID des Fieldobjects zurück gibt.
     *
     * @param id ID des Spielers
     * @return ID des Objektes im Array an der Stelle id-1 (ID 1 = Index 0)
     */
    IFieldObject getPlayerByID(int id) {
        return this.playerListList.get(id - 1);
    }

    /**
     * Methode, welche den Namen des Fieldobjects zurückgibt
     *
     * @param id ID des Spielers
     * @return Namen des Objektes im Array an der Stelle id-1.
     */
    public String getPlayerName(int id) {
        return this.playerListList.get(id - 1).getName();
    }

    public Color getPlayerColor(int id) {return this.playerListList.get(id-1).getColor();}

}

package de.hdm_stuttgart.se2.threads;

import de.hdm_stuttgart.se2.core.GameBoard;
import de.hdm_stuttgart.se2.core.GameLogic;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PrintGameboardToFile extends Thread {

    private static PrintGameboardToFile printGameboard_toFile;

    private GameBoard gameBoard = GameBoard.getGameBoard();
    private FileCreator fileCreator = FileCreator.getFileCreator();
    private GameLogic gameLogic = GameLogic.getGameLogic();
    private Logger log = LogManager.getLogger("logfile");

    /**
     * Erstell Singleton von Thread
     *
     * @return printGameboard_toFile
     */
    public static PrintGameboardToFile getPrintGameboard_toFile() {
        if (printGameboard_toFile == null) {
            printGameboard_toFile = new PrintGameboardToFile();
            return printGameboard_toFile;
        } else {
            return printGameboard_toFile;
        }
    }

    /**
     * Printet alle 5 Sekunden in thread_save.txt das aktuelle GameBoard
     */
    public synchronized void run() {
        log.trace("Thread \"PrintGameboardToFile\" start.");
        while (true) {
            if (gameLogic.isGameRunning()) {
                try {
                    fileCreator.fileCreatorAppend(fileCreator.timeStampCreator(), gameBoardPrint());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private String gameBoardPrint() {
        StringBuilder stringBuilder = new StringBuilder("\n");
        for (int i = 0; i <= gameBoard.getHeight() - 1; i++) {
            for (int j = 0; j <= gameBoard.getWidth() - 1; j++) {
                stringBuilder.append(gameBoard.getCellArray()[j][i].getFieldObject().getId()).append(" ");
            }
            stringBuilder.append("\n");

        }
        return stringBuilder.toString();

    }
}





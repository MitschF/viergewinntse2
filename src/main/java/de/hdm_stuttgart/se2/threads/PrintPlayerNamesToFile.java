package de.hdm_stuttgart.se2.threads;

import de.hdm_stuttgart.se2.core.GameLogic;
import de.hdm_stuttgart.se2.core.PlayerList;

import java.io.IOException;

public class PrintPlayerNamesToFile extends Thread {

    private static PrintPlayerNamesToFile playerNamesThread;
    private FileCreator fileCreator = FileCreator.getFileCreator();
    private GameLogic gameLogic = GameLogic.getGameLogic();

    /**
     * Erstell Singleton von Thread
     * @return playerNamesThread
     */
    public static PrintPlayerNamesToFile getPrintPlayerNamesToFile() {
        if (playerNamesThread == null) {
            playerNamesThread = new PrintPlayerNamesToFile();
            return playerNamesThread;
        } else {
            return playerNamesThread;
        }
    }

    /**
     * Printet alle 6 Sekunden in thread_save.txt die beiden Spielernamen
     */
    public synchronized void run() {
        while (true) {
            while (gameLogic.isGameRunning()) {
                try {
                    fileCreator.fileCreatorAppend("Playernames:", printPlayerNames());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    sleep(6000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Holt die aktuellen Namen aufgrund der Spieler-ID aus Playerlist
     * @return playerName1 und playerName2 als String
     */
    private String printPlayerNames(){
        String player1 = PlayerList.getPlayerList().getPlayerName(1);
        String player2 = PlayerList.getPlayerList().getPlayerName(2);
        return "Spieler 1: "+player1+"; Spieler 2: "+player2+"\n\n";
    }

}

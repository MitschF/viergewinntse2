package de.hdm_stuttgart.se2.threads;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class FileCreator {

    private static FileCreator fileCreator;

    /**
     * Erstellt Singleton vom FileCreator
     * @return fileCreator
     */
    public static FileCreator getFileCreator() {
        if (fileCreator == null) {
            fileCreator = new FileCreator();
            return fileCreator;
        } else {
            return fileCreator;
        }
    }


    public void fileCreatorFlush() throws IOException {
        try {
            Files.deleteIfExists(Paths.get("src/main/resources/thread_save.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Files.createFile(Paths.get("src/main/resources/thread_save.txt"));
    }


    /**
     * Erstellt eine Textdatei mit angegebenem Pfad,
     * welche auch von anderen Methoden abgerufen und befüllt werden kann
     * @param text1 für erstes Attribut
     * @param text2 für zweites Attribut
     * @throws IOException wegen dem Path
     */
    public synchronized void fileCreatorAppend(String text1, String text2) throws IOException {
        List<String> lines = Arrays.asList(text1, text2);
        Path file = Paths.get("src/main/resources/thread_save.txt");
        Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    }

    String timeStampCreator(){
        String timestamp = new SimpleDateFormat("HH:mm:ss dd.MM.yyyy").format(Calendar.getInstance().getTime());
        return "Last save: " + timestamp;
    }
}

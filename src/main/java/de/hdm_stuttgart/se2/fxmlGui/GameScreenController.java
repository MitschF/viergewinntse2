package de.hdm_stuttgart.se2.fxmlGui;

import de.hdm_stuttgart.se2.Exceptions.FXMLLoaderException;
import de.hdm_stuttgart.se2.core.GameBoard;
import de.hdm_stuttgart.se2.core.GameLogic;
import de.hdm_stuttgart.se2.core.PlayerList;
import de.hdm_stuttgart.se2.threads.FileCreator;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.util.Random;

public class GameScreenController {

    /**
     * LabelNummer1 für Spielernamen1, LabelNummer2 für Spielernamen2
     */
    public javafx.scene.control.Label LabelNummer1, LabelNummer2, benachrichtigungslabel;
    /**
     * Buttons für Spalten vom Spielfeld
     */
    public javafx.scene.control.Button Spalte0, Spalte1, Spalte2, Spalte3, Spalte4, Spalte5, Spalte6;

    /**
     * Rectangles in Gridpane = Spielsteine auf Spielfeld
     */
    public Rectangle
            rectangle0_0, rectangle0_1, rectangle0_2, rectangle0_3, rectangle0_4, rectangle0_5,
            rectangle1_0, rectangle1_1, rectangle1_2, rectangle1_3, rectangle1_4, rectangle1_5,
            rectangle2_0, rectangle2_1, rectangle2_2, rectangle2_3, rectangle2_4, rectangle2_5,
            rectangle3_0, rectangle3_1, rectangle3_2, rectangle3_3, rectangle3_4, rectangle3_5,
            rectangle4_0, rectangle4_1, rectangle4_2, rectangle4_3, rectangle4_4, rectangle4_5,
            rectangle5_0, rectangle5_1, rectangle5_2, rectangle5_3, rectangle5_4, rectangle5_5,
            rectangle6_0, rectangle6_1, rectangle6_2, rectangle6_3, rectangle6_4, rectangle6_5;

    /**
     * Label, welches die Anzahl der leeren Felder zeigt.
     */
    public Label labelEmptyFields;


    private PlayerList playerList = PlayerList.getPlayerList();
    private GameBoard gameBoard = GameBoard.getGameBoard();
    private GameLogic gameLogic = GameLogic.getGameLogic();
    private AlertBoxController alertBoxController = new AlertBoxController();
    private Logger log = LogManager.getLogger("logfile");
    private FileCreator fileCreator = FileCreator.getFileCreator();

    /**
     * Initialize-Methode für setText von beiden Labels zu Spielernamen
     */
    @FXML
    public void initialize() throws IOException {
        try {
            gameBoard.setCountTurns(new Random().nextInt(2) + 1);
            LabelNummer1.setText(playerList.getPlayerName(1));
            LabelNummer2.setText(playerList.getPlayerName(2));
            setLabelEmptyFields();
            playerTurn();
            gameLogic.setGameRunning(true);
            gameLogic.setWon(false);
            fileCreator.fileCreatorAppend("______________________\nSpielstart", "______________________");
        }catch (IOException Ie){
            log.fatal("Beim Starten des Spiels im GameScreen ist eine IOException aufgetreten!");
            alertBoxController.IOExceptionThrown();
        }

    }

    /**
     * Beendet das Spiel
     * @param event BeendenButton wird gedrückt
     */
    public void Beenden(ActionEvent event){
        if (alertBoxController.beendenButton()) {
            System.exit(0);
        }
    }

    /**
     * Neustart vom Spiel
     * @param event RestartButton wird geklickt
     * @throws FXMLLoaderException beim Laden des FXMLLoaders
     */
    public void restartButtonClicked(ActionEvent event) throws IOException {
        if (alertBoxController.restartButton()) {
            try {
                gameBoard.createCells();
                Parent startParent = null;
                startParent = FXMLLoader.load(getClass().getResource("/NameScreen.fxml"));
                Scene GoScene = new Scene(startParent);
                Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
                window.setScene(GoScene);
                window.show();
                gameLogic.setGameRunning(false);
                playerList.removePlayer();
            } catch (LoadException fe){
                alertBoxController.FXMLExceptionThrown("'NameScreen.fxml'");
                log.fatal("FXMLLoaderException: FXML-Datei 'NameScreen.fxml' ist fehlerhaft.");
                throw new FXMLLoaderException("Die FXML Datei 'NameScreen.fxml' konnte nicht geladen werden!");
            } catch (IOException e) {
                alertBoxController.IOExceptionThrown();
                log.fatal("Bei Wechsel zu NameScreen ist eine IOException aufgetreten!");
            }
        }
    }

    /**
     * playerTurn-Methode
     * Gibt im benachrichtigungslabel aus welcher Spieler dran ist
     */
    private void playerTurn() {
        if (gameBoard.getCountTurns() % 2 != 0) {
            benachrichtigungslabel.setText(playerList.getPlayerName(2) + " ist dran!");
        } else benachrichtigungslabel.setText(playerList.getPlayerName(1) + " ist dran!");
    }

    /**
     * Mehtode unentschiedenTest
     * Prüft ob Spielfeld voll ist und damit ob das Spiel unentschieden ist
     * @return boolean getRowFull
     */
    private boolean unentschiedenTest() {
        return gameBoard.getRowFull(1) && gameBoard.getRowFull(2) && gameBoard.getRowFull(3)
                && gameBoard.getRowFull(4) && gameBoard.getRowFull(5) && gameBoard.getRowFull(6)
                && gameBoard.getRowFull(7);
    }

    /**
     * Methose disableAllButtons
     * Schaltet, wenn gecallt, alle Buttons aus, mit denen man einen Stein setzen kann
     */
    private void disableAllButtons() {
        Spalte0.setDisable(true);
        Spalte1.setDisable(true);
        Spalte2.setDisable(true);
        Spalte3.setDisable(true);
        Spalte4.setDisable(true);
        Spalte5.setDisable(true);
        Spalte6.setDisable(true);
    }

    /**
     * Methode labelDraw
     * Gibt im benachrichtigungslabel aus, wenn das Spiel unentschieden ist
     */
    private void labelDraw() {
        benachrichtigungslabel.setText("Unentschieden!");
    }

    /**
     * Methode labelWon
     * Gibt im benachrichtigungslabel aus, wenn und von wem das Spiel gewonnen wurde
     */
    private void labelWon() {
        if (gameBoard.getCountTurns() % 2 != 0) {
            benachrichtigungslabel.setText(playerList.getPlayerName(1) + " hat gewonnen!");
        } else benachrichtigungslabel.setText(playerList.getPlayerName(2) + " hat gewonnen!");
    }

    /**
     * spieler2Rectangle und spieler1Rectangle um gewähltes Spielfeld mit Spielerfarbe einzufärben
     * @param rec1 Erstes Rectangle der Spalte
     * @param rec2 Zweites Rectangle der Spalte
     * @param rec3 Drittes Rectangle der Spalte
     * @param rec4 Viertes Rectangle der Spalte
     * @param rec5 Fünftes Rectangle der Spalte
     * @param rec6 Sechstes Rectangle der Spalte
     */
    private void spieler2Rectangle(Rectangle rec1, Rectangle rec2, Rectangle rec3, Rectangle rec4,
                                   Rectangle rec5, Rectangle rec6){
        if (gameBoard.getY() == 5) rec1.setFill(playerList.getPlayerColor(2));
        else if (gameBoard.getY() == 4) rec2.setFill(playerList.getPlayerColor(2));
        else if (gameBoard.getY() == 3) rec3.setFill(playerList.getPlayerColor(2));
        else if (gameBoard.getY() == 2) rec4.setFill(playerList.getPlayerColor(2));
        else if (gameBoard.getY() == 1) rec5.setFill(playerList.getPlayerColor(2));
        else if (gameBoard.getY() == 0) rec6.setFill(playerList.getPlayerColor(2));
    }

    private void spieler1Rectangle(Rectangle rec1, Rectangle rec2, Rectangle rec3, Rectangle rec4,
                                   Rectangle rec5, Rectangle rec6){
        if (gameBoard.getY() == 5) rec1.setFill(playerList.getPlayerColor(1));
        else if (gameBoard.getY() == 4) rec2.setFill(playerList.getPlayerColor(1));
        else if (gameBoard.getY() == 3) rec3.setFill(playerList.getPlayerColor(1));
        else if (gameBoard.getY() == 2) rec4.setFill(playerList.getPlayerColor(1));
        else if (gameBoard.getY() == 1) rec5.setFill(playerList.getPlayerColor(1));
        else if (gameBoard.getY() == 0) rec6.setFill(playerList.getPlayerColor(1));
    }

    /**
     * DropCoinSpalte1, ..., DropCoinSpalte7 um Spielfeld in ausgewählter Reihe
     * mit richtiger Spielerfarbe anzuzeigen
     * gameBoard.getCountTurns();gameBoard.dropCoin();spieler2Rectangle();spieler1Rectangle();
     * gameBoard.printGameboard()
     * @param actionEvent Button drücken um Spalte auszuwählen und Stein zu platzieren
     */
    public void DropCoinSpalte1(ActionEvent actionEvent) {
        if (gameBoard.getCountTurns() % 2 != 0) {
            gameBoard.dropCoin(1, 2);
            spieler2Rectangle(rectangle0_5,rectangle0_4,rectangle0_3,rectangle0_2,rectangle0_1,rectangle0_0);
            gameBoard.printGameboard();
        } else {
            gameBoard.dropCoin(1, 1);
            spieler1Rectangle(rectangle0_5,rectangle0_4,rectangle0_3,rectangle0_2,rectangle0_1,rectangle0_0);
            gameBoard.printGameboard();
        }
        setLabelEmptyFields();
        playerTurn();
        gameLogic.checkWin();
        if (gameLogic.getWon()) {
            disableAllButtons();
            labelWon();
        }
        if (gameBoard.getRowFull(1)) {
            log.trace("Spalte 1 voll.");
            Spalte0.setDisable(true);
        }
        if (unentschiedenTest()) {
            disableAllButtons();
            labelDraw();
        }
    }

    public void DropCoinSpalte2(ActionEvent actionEvent) {
        if (gameBoard.getCountTurns() % 2 != 0) {
            gameBoard.dropCoin(2, 2);
            spieler2Rectangle(rectangle1_5,rectangle1_4,rectangle1_3,rectangle1_2,rectangle1_1,rectangle1_0);
            gameBoard.printGameboard();
        } else {
            gameBoard.dropCoin(2, 1);
            spieler1Rectangle(rectangle1_5,rectangle1_4,rectangle1_3,rectangle1_2,rectangle1_1,rectangle1_0);
            gameBoard.printGameboard();
        }
        setLabelEmptyFields();
        playerTurn();
        gameLogic.checkWin();
        if (gameLogic.getWon()) {
            disableAllButtons();
            labelWon();
        }
        if (gameBoard.getRowFull(2)) {
            log.trace("Spalte 2 voll.");
            Spalte1.setDisable(true);
        }
        if (unentschiedenTest()) {
            disableAllButtons();
            labelDraw();
        }
    }

    public void DropCoinSpalte3(ActionEvent actionEvent) {
        if (gameBoard.getCountTurns() % 2 != 0) {
            gameBoard.dropCoin(3, 2);
            spieler2Rectangle(rectangle2_5,rectangle2_4,rectangle2_3,rectangle2_2,rectangle2_1,rectangle2_0);
            gameBoard.printGameboard();
        } else {
            gameBoard.dropCoin(3, 1);
            spieler1Rectangle(rectangle2_5,rectangle2_4,rectangle2_3,rectangle2_2,rectangle2_1,rectangle2_0);
            gameBoard.printGameboard();
        }
        playerTurn();
        setLabelEmptyFields();
        gameLogic.checkWin();
        if (gameLogic.getWon()) {
            disableAllButtons();
            labelWon();
        }
        if (gameBoard.getRowFull(3)) {
            log.trace("Spalte 3 voll.");
            Spalte2.setDisable(true);
        }
        if (unentschiedenTest()) {
            disableAllButtons();
            labelDraw();
        }
    }

    public void DropCoinSpalte4(ActionEvent actionEvent) {
        if (gameBoard.getCountTurns() % 2 != 0) {
            gameBoard.dropCoin(4, 2);
            spieler2Rectangle(rectangle3_5,rectangle3_4,rectangle3_3,rectangle3_2,rectangle3_1,rectangle3_0);
            gameBoard.printGameboard();
        } else {
            gameBoard.dropCoin(4, 1);
            spieler1Rectangle(rectangle3_5,rectangle3_4,rectangle3_3,rectangle3_2,rectangle3_1,rectangle3_0);
            gameBoard.printGameboard();
        }
        setLabelEmptyFields();
        playerTurn();
        gameLogic.checkWin();
        if (gameLogic.getWon()) {
            disableAllButtons();
            labelWon();
        }
        if (gameBoard.getRowFull(4)) {
            log.trace("Spalte 4 voll.");
            Spalte3.setDisable(true);
        }
        if (unentschiedenTest()) {
            disableAllButtons();
            labelDraw();
        }
    }

    public void DropCoinSpalte5(ActionEvent actionEvent) {
        if (gameBoard.getCountTurns() % 2 != 0) {
            gameBoard.dropCoin(5, 2);
            spieler2Rectangle(rectangle4_5,rectangle4_4,rectangle4_3,rectangle4_2,rectangle4_1,rectangle4_0);
            gameBoard.printGameboard();
        } else {
            gameBoard.dropCoin(5, 1);
            spieler1Rectangle(rectangle4_5,rectangle4_4,rectangle4_3,rectangle4_2,rectangle4_1,rectangle4_0);
            gameBoard.printGameboard();
        }
        setLabelEmptyFields();
        playerTurn();
        gameLogic.checkWin();
        if (gameLogic.getWon()) {
            disableAllButtons();
            labelWon();
        }
        if (gameBoard.getRowFull(5)) {
            log.trace("Spalte 5 voll.");
            Spalte4.setDisable(true);
        }
        if (unentschiedenTest()) {
            disableAllButtons();
            labelDraw();
        }
    }

    public void DropCoinSpalte6(ActionEvent actionEvent) {
        if (gameBoard.getCountTurns() % 2 != 0) {
            gameBoard.dropCoin(6, 2);
            spieler2Rectangle(rectangle5_5,rectangle5_4,rectangle5_3,rectangle5_2,rectangle5_1,rectangle5_0);
            gameBoard.printGameboard();
        } else {
            gameBoard.dropCoin(6, 1);
            spieler1Rectangle(rectangle5_5,rectangle5_4,rectangle5_3,rectangle5_2,rectangle5_1,rectangle5_0);
            gameBoard.printGameboard();
        }
        setLabelEmptyFields();
        playerTurn();
        gameLogic.checkWin();
        if (gameLogic.getWon()) {
            disableAllButtons();
            labelWon();
        }
        if (gameBoard.getRowFull(6)) {
            log.trace("Spalte 6 voll.");
            Spalte5.setDisable(true);
        }
        if (unentschiedenTest()) {
            disableAllButtons();
            labelDraw();
        }
    }

    public void DropCoinSpalte7(ActionEvent actionEvent) {
        if (gameBoard.getCountTurns() % 2 != 0) {
            gameBoard.dropCoin(7, 2);
            spieler2Rectangle(rectangle6_5,rectangle6_4,rectangle6_3,rectangle6_2,rectangle6_1,rectangle6_0);
            gameBoard.printGameboard();
        } else {
            gameBoard.dropCoin(7, 1);
            spieler1Rectangle(rectangle6_5,rectangle6_4,rectangle6_3,rectangle6_2,rectangle6_1,rectangle6_0);
            gameBoard.printGameboard();
        }
        setLabelEmptyFields();
        playerTurn();
        gameLogic.checkWin();
        if (gameLogic.getWon()) {
            disableAllButtons();
            labelWon();
        }
        if (gameBoard.getRowFull(7)) {
            log.trace("Spalte 7 voll.");
            Spalte6.setDisable(true);
        }
        if (unentschiedenTest()) {
            disableAllButtons();
            labelDraw();
        }
    }

    /**
     * Zählt leere Felder auf Gameboard und gibt diese im Label aus
     */
    public void setLabelEmptyFields() {
        labelEmptyFields.setText("Noch " + gameBoard.countEmptyFields() + " leere Felder");
    }
}

package de.hdm_stuttgart.se2.fxmlGui;

import de.hdm_stuttgart.se2.Exceptions.FXMLLoaderException;
import de.hdm_stuttgart.se2.threads.FileCreator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class StartScreen extends Application {

    private AlertBoxController alertBoxController = new AlertBoxController();
    private Logger log = LogManager.getLogger("logfile");

    /**
     *Erste Szene mit StartScreen.fxml
     * @param primaryStage Stage die als erstes als StartScreen angezeigt wird
     * @throws IOException FXMLLoaderException oder IOException beim Laden der FXML
     */
    @Override
    public void start(Stage primaryStage) throws IOException {

        try {
            Parent root = FXMLLoader.load(getClass().getResource(
                    "/StartScreen.fxml"));
            primaryStage.setTitle("4 Gewinnt");
            primaryStage.setScene(new Scene(root, 800, 500));
            primaryStage.show();
        }catch (LoadException le){
            alertBoxController.FXMLExceptionThrown("'GameScreen.fxml'");
            log.fatal("FXMLLoaderException: FXML-Datei 'StartScreen.fxml' ist fehlerhaft.");
            throw new FXMLLoaderException("Die FXML Datei 'StartScreen.fxml' konnte nicht geladen werden!");
        }catch (IOException ie){
            alertBoxController.IOExceptionThrown();
            log.fatal("Beim Laden von StartScreen ist eine IOException aufgetreten!");
        }
    }

    /**
     * Main-Methode des Programms, das die Anwendung startet
     * @param args gehört zur Main-Methode
     */
    public static void main(String[] args) {

        try {
            FileCreator.getFileCreator().fileCreatorFlush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        launch(args);


    }


}

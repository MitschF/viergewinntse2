package de.hdm_stuttgart.se2.fxmlGui;


import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;


class AlertBoxController {

    private Logger log = LogManager.getLogger("logfile");

    /**
     * Untersucht ob eingegebene Strings gleich sind und zeigt (falls false) Alert-Fenster an
     *
     * @param text1 als Textfeld für Namen1
     * @param text2 als Textfeld für Namen2
     * @return boolean
     */
    boolean fehlerGleicherName(String text1, String text2) {
        StringBuilder errors = new StringBuilder();

        if (text1.equalsIgnoreCase(text2) || text2.equalsIgnoreCase(text1)) {
            log.warn("Gleiche Namen für beide Spieler eingegeben.");
            errors.append("Bitte unterschiedliche Namen für beide Spieler eingeben!");
        }
        if (errors.length() > 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fehlermeldung");
            alert.setHeaderText("Die Namen für Spieler 1 und Spieler 2 sind gleich!");
            alert.setContentText(errors.toString());

            alert.showAndWait();
            return false;
        } else return true;
    }

    /**
     * Untersucht ob eingegebene Strings leer bzw. vorhanden sind und zeigt (falls false) Alert-Fenster an
     *
     * @param text1 als Textfeld für Name1
     * @param text2 als Textfeld für Name2
     * @return boolean
     */
    boolean fehlerKeinName(String text1, String text2) {

        StringBuilder errors = new StringBuilder();

        if (text1.trim().isEmpty()) {
            log.warn("Kein Name für Spieler 1 eingegeben.");
            errors.append("Bitte für Spieler 1 einen Namen eingeben!\n");
        }
        if (text2.trim().isEmpty()) {
            log.warn("Kein Name für Spieler 2 eingegeben.");
            errors.append("Bitte für Spieler 2 einen Namen eingeben!");
        }

        if (errors.length() > 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fehlermeldung");
            alert.setHeaderText("Die benötigten Namenfelder sind leer.");
            alert.setContentText(errors.toString());

            alert.showAndWait();
            return false;
        } else return true;
    }

    /**
     * ALert-Fenster, das auftaucht, wenn Restart-Button gedrückt wird
     *
     * @return boolean
     */
    boolean restartButton() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Sind sie sicher?");
        alert.setHeaderText("Wollen Sie das Spiel wirklich neu starten?");

        ButtonType yesButton = new ButtonType("Ja");
        ButtonType noButton = new ButtonType("Nein", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(yesButton, noButton);
        Optional<ButtonType> result = alert.showAndWait();


        if (result.get() == yesButton) {
            log.debug("Spiel neu gestartet.");
            return true;
        } else return false;
    }

    /**
     * Alert-Fenster, das auftaucht, wenn Beenden-Button gedrückt wird
     *
     * @return boolean
     */
    boolean beendenButton() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Sind Sie sicher?");
        alert.setHeaderText("Wollen Sie das Spiel wirklich beenden?");

        ButtonType yesButton = new ButtonType("Ja");
        ButtonType noButton = new ButtonType("Nein", ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(yesButton, noButton);
        Optional<ButtonType> result = alert.showAndWait();


        if (result.get() == yesButton) {
            log.debug("Spiel beendet.");
            return true;
        } else return false;
    }

    /**
     * Alert-Box anzeigen, wenn die FXMLLoaderException auftritt
     * @param fxmlDatei für Namen von fehlerhafter FXML-Datei
     */
    void FXMLExceptionThrown(String fxmlDatei) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        log.fatal("FXMLLoaderException wurde ausgelöst!");
        alert.setTitle("Ein Fehler ist aufgetreten");
        alert.setHeaderText("\nERROR-NAME: FXML-Loader-Exception\n\n"+
                fxmlDatei + " konnte entweder nicht gefunden werden oder ist fehlerhaft!\n"
                +"Bitte beenden Sie das Spiel und wenden sich mit dem oben\n"
                +"dargestellten ERROR-Namen an Ihr kompetentes IT-Team.");
        alert.showAndWait();
    }

    /**
     * Alert-Box anzeigen, wenn eine IOException auftritt
     */
    void IOExceptionThrown(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        log.fatal("IOException ist aufgetreten!");
        alert.setTitle("Ein Fehler ist aufgetreten");
        alert.setHeaderText("\nERROR-NAME: IOException\n\n"
                +"Bitte beenden Sie das Spiel und wenden sich mit dem oben\n"
                +"dargestellten ERROR-Namen an Ihr kompetentes IT-Team.");
        alert.showAndWait();
    }
}

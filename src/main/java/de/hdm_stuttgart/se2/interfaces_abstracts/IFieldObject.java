package de.hdm_stuttgart.se2.interfaces_abstracts;

import javafx.scene.paint.Color;

public interface IFieldObject {

    int getId();

    Color getColor();

    String getName();

}

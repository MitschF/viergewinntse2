package de.hdm_stuttgart.se2.interfaces_abstracts;


import javafx.scene.paint.Color;

public abstract class FieldObject implements IFieldObject {
    protected int id;
    protected Color color;
    protected String name;

    public int getId() {
        return this.id;
    }

    public Color getColor() {
        return this.color;
    }

    public String getName() {
        return this.name;
    }

}

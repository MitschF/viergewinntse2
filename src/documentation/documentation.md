# Vier Gewinnt

## Wintersemester 2018/2019 - Softwareentwicklung 2

Link zum Projekt:  
[https://gitlab.mi.hdm-stuttgart.de/SoftwareDevelopment1/viergewinntse2.git](https://gitlab.mi.hdm-stuttgart.de/SoftwareDevelopment1/viergewinntse2.git)

  Michele Fliedner, Email: mf155@hdm-stuttgart.de  
  Niclas Schmidt, Email: ns120@hdm-stuttgart.de  
  Teresa Mary Kutzner, Email: tk145@hdm-stuttgart.de  
  Philipp Umscheid, Email: pu008@hdm-stuttgart.de  

## Kurzbeschreibung

Bei unserem Projekt handelt es sich um ein 4-Gewinnt Spiel im Rahmen der Vorlesung Software-Entwicklung 2.
Ziel des Spiels ist es, als erster 4 Steine in eine Reihe (horizontal, vertikal oder diagonal) zu platzieren. Man Spielt zu zweit und ist abwechselnd am Zug, einen Stein zu platzieren.
Ein Stein rutscht, anders als bei Tic-Tac-Toe, in der ausgewählten Spalte so weit wie möglich nach unten. 

Das Spiel ist dann beendet, wenn ein Spieler zu erst 4 Steine in einer Reihe platziert, oder wenn das Spielfeld voll ist (unentschieden).

## Startklasse
Die Main-Methode zum Starten der Applikation befindet sich in der Klasse **StartScreen** im fxmlGui-Package.
## Besonderheiten
Im Projekt sind nicht genutzte Methoden vorhanden. Wir haben uns entschieden, diese im Programm zu lassen, da sie beim weiteren Arbeiten an diesem Projekt gebraucht werden können.
## Use-Case- und UML Klassendiagramm
Das UML-Klassendiagramm wie auch das Use-Case-Diagramm finden Sie zusätzlich im git-Repository im Ordner
documentation/resources als [UML_UseCase_VierGewinnt.jpeg](resources/UML_UseCase_VierGewinnt.jpeg) oder auf [Lucidchart](https://www.lucidchart.com/invitations/accept/77954689-e307-4632-8752-5af61d95bcc8).

![UML-Diagramm](resources/UML_UseCase_VierGewinnt.jpeg)
## Stellungnahme
### Architektur  
Im Ordnerpfad viergewinntse2/src/main/java/de.hdm_stuttgart.se2 ist unser Projekt mit allen Klassen hinterlegt.
Hier haben wir sechs Packages:
- Das **Core**-Package beinhaltet die Spielelogik mit den Klassen *Cell*, *GameBoard*, *GameLogic* und *PlayerList*.
Hier wird die Logik der Hauptsteuerung verarbeitet. In der Klasse *GameBoard* wird außerdem unser Stream verarbeitet.
- Das **Exception**-Package enthält unsere eigens erstellte Exception "FXMLLoaderException" als Klasse.
- Das **fieldObjects**-Package besteht aus drei Klassen: *EmptyField*, *FieldObjectFactory* und *Player*.
Hier werden mit einer Factory weitere Objekte für die Spiellogik erstellt, die sich als Spieler und leeres Feld auszeichnen.
- Das **fxmlGui**-Package besteht aus fünf Klassen, welche sich alle auf den Resources-Ordner [resources](https://gitlab.mi.hdm-stuttgart.de/SoftwareDevelopment1/viergewinntse2/blob/master/src/main/resources) und die darin enthaltenen
FXML-Dateien beziehen.  
Hier ist neben den Klassen für das Graphic-User-Interface auch unsere **Main-Klasse [Startscreen](https://gitlab.mi.hdm-stuttgart.de/SoftwareDevelopment1/viergewinntse2/blob/master/src/main/java/de/hdm_stuttgart/se2/fxmlGui/StartScreen.java)** dabei,
die das Programm startet. Weitere Klassen darin sind: *AlertBoxController*, *GameScreenController*, *NameScreenController* und *StartScreenController*,
die alle als Controller für die GUI fungieren.
- Das **interfaces_abstracts**-Package enthält eine abstrakte Klasse namens *FieldObject* und ein Interface *IFieldObject*.
Dabei implementiert das Interface *IFieldObject* die abstrakte Klasse *FieldObject*, von welcher die beiden Klassen *Player* und *EmptyField* im *fieldObjects-Package* erben.
- Das **threads**-Package enthält die drei Klassen *FileCreator*, *LastGameboard_Thread* und *PlayerNames_Thread*.
Hier werden zwei Threads erstellt, wie auch eine Textdatei [thread_save.txt](../main/resources/thread_save.txt) in welche beide Threads
synchronisierte Inhalte einfügen.

Die Package-Struktur unseres Projektes kam aus der Trennung von Logik, GUI und anderen Komponenten hervor.
Wichtig war uns dabei immer den Überblick mit einer sinnigen Sortierung zu behalten. So ist die Logik des
Spiels im core-Package, die GUI im fxmlGui-Package und Exceptions, Threads, fieldObjects und Interfaces/abstrakte
Klassen haben jeweils ebenfalls eigene Packages bekommen. Dies war auch hilfreich für organisatorische Zwecke rund um das Projekt.
### Clean Code  
Statische Felder im Code wurden immer auf *private* gestellt, da sie nur in ihrer Klasse benutzt wurden. *Public*-Member wurden nur 
für Methoden benutzt, damit man sie in anderen Klassen abrufen kann.  
### Exceptions
Die Exceptions, welche im Verlauf der Programmerstellung aufgetreten sind, konnten wir erfolgreich 
mit Hilfe der GUI abfangen und lösen. Dennoch haben wir eine eigene Exception erstellt, die ausgelöst wird,
wenn eine FXML-Datei fehlerhaft ist. Die "FXMLLoaderException" wird nicht nur als solche geloggt, sondern hat
auch ein eigenes Alert-Fenster, sodass der Benutzer benachrichtigt wird, dass sein Programm beendet werden sollte.
### Grafische Oberfläche
Die GUI wurde mit Hilfe von dem Programm Scenebuilder erstellt und besteht aus 3 FXML-Dateien, 4 Controllern und einer Start-Klasse.  
Die Controller wie auch die Start-Klasse sind im *fxmlGui*-Package, die Fxml-Dateien selber im *resources*-Ordner.
Bei der Gestaltung der grafischen Oberfläche war es uns sehr wichtig dem Nutzer das Spielen so einfach wie möglich zu gestalten.
Dabei legten wir auch viel Wert auf Alertboxen und die verständliche Darstellung für den Spieler. 
### Logging
Beim Logging haben wir uns hauptsächlich für die Log-Level *DEBUG* und *TRACE* entschieden um den Spielablauf
und wichtige Ereignisse, wie das Erstellen der Spieler, festzuhalten. Fehlerhafte Eingaben des Nutzers haben wir mit dem *WARN*-Level geloggt
und die eigens erstellte FXMLLoaderException wird mit dem *FATAL*-Level geloggt.
### Threads
Das Arbeiten mit Threads war in unserem Projekt nicht zwingend für den Spielablauf erforderlich. Deswegen haben wir
zwei synchronisierte Threads (Klassen*LastGameBoard_Thread* und *PlayerNames_Thread*) erstellt, die den Spielstand, wie auch die Namen der Spieler in Zeitintervallen in eine gleiche Textdatei
thread_save.txt schreiben. Die Textdatei wird dabei durch die Klasse *FileCreator* erstellt.  
Dadurch kann man die Spielverläufe, wie auch die Spieler im Nachhinein in der Textdatei mit Zeitstempeln einsehen. 
### Streams und Lambda-Funktionen
Unser Stream wird für die Zahlangabe von leeren Feldern im Spielfeld (Klasse *GameBoard*) genutzt. Dadurch filtert der Stream das Cell-Array des Spielfelds auf leere Felder mit Hilfe
einer Lambda-Funktion. Wie viele Felder noch leer sind, wird auch für den Spieler während des Spiels als Label ausgegeben.
### Dokumentation und Test-Fälle
In den Klassen wurden JDocs für die Dokumentation und für die Erklärung des Codes geschrieben.
Die Tests wurden durch JUnit Tests im Ordner [test](https://gitlab.mi.hdm-stuttgart.de/SoftwareDevelopment1/viergewinntse2/tree/master/src/test/java/de/hdm_stuttgart/se2) erstellt.
Daher, dass einige unserer Methoden einen *void*-Rückgabewert haben, konnten wir dafür keine Tests erstellen.
Trotzdem haben unsere wichtigsten Methoden normale, wie auch Negativ-Tests.
### Factories
Die Factory unseres Programms heißt [FieldObjectFactory](https://gitlab.mi.hdm-stuttgart.de/SoftwareDevelopment1/viergewinntse2/blob/master/src/main/java/de/hdm_stuttgart/se2/fieldObjects/FieldObjectFactory.java) und ist im Package *fieldObjects* zu finden.
Sie wird benutzt um die einzelnen Spieler mit einer ID (1 oder 2), einem Namen und einer Farbe zu erstellen. Dadurch gibt sie entweder ein FieldObject namens *Player* für Spieler, oder ein leeres Feld
zurück namens *EmptyField*. Beide der zurückgegebenen Objekte sind auch als Klassen (*Player* und *EmptyField*) im gleichen Package zu finden.
### UML
Das Klassendiagramm unseres Projektes wurde so gestaltet, dass die Vernetzung der Klassen im allgemeinen dargestellt wird. Dabei wurde auf eine ausführliche
Darstellung aller GUI-Komponenten verzichtet und nur ein paar als Exempel dargestellt um das Diagramm so übersichtlich wie möglich zu halten.
Das Use-Case-Diagramm ist aus der Sicht des Nutzers ertellt worden und beschreibt den Ablauf unseres Spieles mit dem User zusammen.
## Bewertungsbogen
Den Bewertungsbogen finden Sie zusätzlich im git-Repository im Ordner documentation/resources als [Projektnoten_template.xlsx](resources/Projektnoten_template%20.xlsx)
![Bewetungsbogen](resources/Bewertung.png)
## Profiling Analyse
Das Bild unserer Profiling-Analyse finden Sie zusätzlich im git-Repository im Ordner documentation/resources als [Profiling_Analyse.jpeg](resources/Profiling_Analyse.jpeg)
![Profiling-Analyse](resources/Profiling_Analyse.jpeg)  
Der Screenshot zeigt das Profiling vom Start des Programmes bishin zum Ablauf der ersten Spielzüge. Zu Erkennen ist zum Beispiel der Rückgang des Speicherverbrauchs
sobald der Garbage-Collector läuft. Ebenso kann man sehen, wie die Threads unseres Spiels erst Anfangen, wenn der Spieler zum Spielfeld (*GameScreen*) und damit zum eigentlichen Spiel gelangt.
